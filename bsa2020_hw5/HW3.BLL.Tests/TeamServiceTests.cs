﻿using AutoMapper;
using HW3.BLL.Services;
using HW3.BLL.ServicesAbstraction;
using HW3.Common;
using HW3.Common.DTO;
using HW3.Common.Exeptions;
using HW3.DAL.Abstracts;
using HW3.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Sdk;

namespace HW3.BLL.Tests
{
    public class TeamServiceTests:IDisposable
    { 
        TeamService teamService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        FakeDbContext context = new FakeDbContext();
        IUserService userService;
        public TeamServiceTests()
        {
            _unitOfWork = new UnitOfWork(context);
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ConfigurationMapper());
            });
            _mapper = new MapperConfiguration(c =>

          c.AddProfile<ConfigurationMapper>()).CreateMapper();
            teamService = new TeamService(_unitOfWork, _mapper);
            userService = new UserService(_unitOfWork, _mapper);
        }
        public void Dispose()
        {
            teamService.Dispose();
        }

        [Theory]
        [InlineData(2,5)]
        [InlineData(25, 53)]
        public void AddPlayersToTeam_WhenUserId2TeamId5_ThenTeamId5(int userId,int teamId)
        {
            var team = new TeamDTO() {Id=teamId, Name = "TestName" };
            teamService.CreateTeam(team);

            var user = new UserDTO() { Id = userId, FirstName = "FirstName", LastName = "LastName" };
            userService.CreateUser(user);

            var count = teamService.GetTeamById(team.Id).UserList.Count;
            teamService.AddPlayersToTeam(userId, team.Id);
            Assert.Equal(count + 1, teamService.GetTeamById(team.Id).UserList.Count);
        }
        [Theory]
        [InlineData(25, 53)]
        public void AddPlayersToTeam_WhenUserNotExist_ThenNotFoundException(int userId, int teamId)
        {
            var team = new TeamDTO() { Id = teamId, Name = "TestName" };
            teamService.CreateTeam(team);

            var count = teamService.GetTeamById(team.Id).UserList.Count;
            Assert.Throws<NotFoundException>(()=>teamService.AddPlayersToTeam(userId, team.Id));
        }
        [Fact]//Task 4
        public void GetListUserByTeamAndMoreThenTenYearsOld_WhenUserRregisterNowAndTeamIdMinimum_ThenUserIsFirst()
        {
            var team1 = new TeamDTO() { Id = 1, Name = "Team1" };
            var team2 = new TeamDTO() { Id = 2, Name = "Team2" };
            teamService.CreateTeam(team1);
            teamService.CreateTeam(team2);

            var user1 = new UserDTO() { Id=1, FirstName="Test", LastName="Test",Birthday = DateTime.Parse("2/10/2009"), TeamId=1, RegisteredAt=DateTime.Parse("2/10/2020") };
            var user2 = new UserDTO() { Id = 2, FirstName = "Test", LastName = "Test", Birthday = DateTime.Parse("2/10/2008"), TeamId = 2, RegisteredAt = DateTime.Parse("2/10/1019") };
            var user3 = new UserDTO() { Id = 3, FirstName = "Test", LastName = "Test", Birthday = DateTime.Parse("2/10/2009"), TeamId = 1, RegisteredAt = DateTime.Parse("5/11/2020") };
            userService.CreateUser(user1);
            userService.CreateUser(user2);
            userService.CreateUser(user3);

            teamService.AddPlayersToTeam(user1.Id,1);
            teamService.AddPlayersToTeam(user2.Id, 2);
            teamService.AddPlayersToTeam(user3.Id, 1);

            var teamPlayers =teamService.GetListUserByTeamAndMoreThenTenYearsOld().First();
            Assert.Equal(user3.Id, teamPlayers.ListUser.First().Id);
        }
        [Fact]
        public void CreateTeam_WhenTeamExist_ThenException()
        {
            var team = new TeamDTO() { Id = 1, Name = "Team" };
            teamService.CreateTeam(team);
            Assert.Throws<Exception>(() => teamService.CreateTeam(team));
        }
    }
}
