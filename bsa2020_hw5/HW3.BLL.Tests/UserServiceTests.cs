using AutoMapper;
using HW3.Common.DTO;
using HW3.BLL.Services;
using HW3.DAL.Abstracts;
using HW3.DAL.Models;
using HW3.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.InMemory;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;
using HW3.Common;
using FakeItEasy;
using HW3.BLL.ServicesAbstraction;
using System.Collections.Generic;
using HW3.Common.Exeptions;

namespace HW3.BLL.Tests
{
    public class UserServiceTests:IDisposable
    {
        private readonly IUserService userService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly FakeDbContext context;
        private readonly ITaskService taskService;
        private readonly IProjectService projectService;
        public UserServiceTests()
        {
            context = new FakeDbContext();
            _unitOfWork = new UnitOfWork(context);
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ConfigurationMapper());
            });
            _mapper = new MapperConfiguration(c =>
                c.AddProfile<ConfigurationMapper>()).CreateMapper();
            userService = new UserService(_unitOfWork, _mapper);
            taskService = new TaskService(_unitOfWork, _mapper);
            projectService = new ProjectService(_unitOfWork, _mapper);
        }

        public void Dispose()
        {
            userService.Dispose();
        }
        [Fact]
        public void CreateUser_WhenNewUser_ThenUsersPlusOne()
        {
            int count = userService.GetUsers().Count;
            UserDTO user = new UserDTO() {FirstName="TestFirstName",LastName="TestLastName"};
            userService.CreateUser(user);
            Assert.Equal(count+1,userService.GetUsers().Count);
           // A.CallTo(() => userService.CreateUser(user)).MustHaveHappened();
        }

       [Fact]//Task 5
       public void GetListUserByFirstName_WhenCallListUserByFirstName_ThenEqualType()
       {
            var listUser = userService.GetListUserByFirstName();
            Assert.True(listUser.GetType() == typeof(List<UserDTO>));
       }

        
        [Theory]
        [InlineData(5)]//Task 6
        public void GetInfoAboutLastProjectByUserId_WhenAuthorId2_ThenCountNotFinishedOrCanceledTasks1(int authorId)
        {
            var user = new UserDTO() { Id = authorId, FirstName = "Test", LastName = "Test" };
            userService.CreateUser(user);
            var project = new ProjectDTO() { Id = 1, Description = "Name", Name = "Name" };
            projectService.CreateProject(project);
            taskService.CreateTask(new TaskDTO() { Id = 4, Description="Test", PerformerId=authorId, ProjectId=1 });
            taskService.CreateTask(new TaskDTO() { Id = 5, Description = "Test2", PerformerId=authorId, ProjectId=1 });
            userService.UpdateUser(user);
        
            var result = userService.GetInfoAboutLastProjectByUserId(authorId);
            Assert.Equal(2, result.CountNotFinishedOrCanceledTasks);
        }


        [Theory]
        [InlineData(2)]
        [InlineData(4)]
        public void GetNotFinishedTasksForUser_When_Then(int performerId)
        {
            //Arrange
            var user = new UserDTO() { Id = performerId, FirstName = "Name", LastName = "LastName" };
            var notPerformer = new UserDTO() { Id = 1, FirstName = "Negative", LastName = "Negative" };
            userService.CreateUser(user);
            userService.CreateUser(notPerformer);

            var project1 = new ProjectDTO() { Id = 1, Name = "Test1", Description = "Test1" };
            var project2 = new ProjectDTO() { Id = 2, Name = "Test2", Description = "Test2" };
            var project3 = new ProjectDTO() { Id = 3, Name = "Test3", Description = "Test3" };
            projectService.CreateProject(project1);
            projectService.CreateProject(project2);
            projectService.CreateProject(project3);

            var taskPositive = new TaskDTO() { Id = 1, Description = "Test", State = 1, PerformerId = performerId, ProjectId = project1.Id };
            var taskPositive2 = new TaskDTO() { Id = 2, Description = "TestPositive", State = 4, PerformerId = performerId, ProjectId = project2.Id };
            var taskNegative = new TaskDTO() { Id = 3, Description = "TestNegative", State = 2, PerformerId = performerId, ProjectId = project1.Id };
            var taskNegative2 = new TaskDTO() { Id = 4, Description = "TestNegative2", State = 1, PerformerId = notPerformer.Id, ProjectId = project3.Id };
            var taskNegative3 = new TaskDTO() { Id = 5, Description = "TestNegative3", ProjectId = project3.Id };
            taskService.CreateTask(taskPositive);
            taskService.CreateTask(taskPositive2);
            taskService.CreateTask(taskNegative);
            taskService.CreateTask(taskNegative2);
            taskService.CreateTask(taskNegative3);

            var result = userService.GetNotFinishedTasksForUser(performerId);//Act

            Assert.Equal(2, result.Count);//Assert
        }
        [Theory]
        [InlineData(10)]
        public void GetNotFinishedTasksForUser_WhenUserNotExist_ThenNotFoundExceprion(int performerId)
        {
            Assert.Throws<NotFoundException>(() => userService.GetNotFinishedTasksForUser(performerId));
        }
    }
}
