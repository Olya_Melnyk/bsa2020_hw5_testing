﻿using HW3.Common.DTO;
using HW3.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HW3.BLL.ServicesAbstraction
{
    public interface IProjectService:IDisposable
    {
        void DeleteProject(int id);
        void DeleteProject(ProjectDTO project);
        void UpdateProject(ProjectDTO project);
        void CreateProject(ProjectDTO project);
        List<ProjectDTO> GetProjects();
        ProjectDTO GetProjectById(int id);
        List<AboutProjectDTO> GetInfoAboutProjects();
        Dictionary<int, int> GetCountTasksByUser(int authorId);
    }
}
