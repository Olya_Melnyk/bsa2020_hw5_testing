﻿using HW3.Common.DTO;
using HW3.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HW3.BLL.ServicesAbstraction
{
    public interface IUserService:IDisposable
    {
        void DeleteUser(int id);
        void DeleteUser(UserDTO user);
        void UpdateUser(UserDTO user);
        void CreateUser(UserDTO user);
        List<UserDTO> GetUsers();
        UserDTO GetUserById(int id);
        List<UserDTO> GetListUserByFirstName();
        AboutLastProjectDTO GetInfoAboutLastProjectByUserId(int authorId);
        List<TaskDTO> GetNotFinishedTasksForUser(int performerId);

    }
}
