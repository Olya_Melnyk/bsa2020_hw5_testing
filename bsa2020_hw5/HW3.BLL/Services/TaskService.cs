﻿using HW3.BLL.ServicesAbstraction;
using HW3.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using HW3.DAL.Repositories;
using HW3.Common.DTO;
using System.Linq;
using HW3.DAL.Abstracts;
using HW3.Common.Exeptions;

namespace HW3.BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<TaskModel> _repository;
        private readonly IUnitOfWork _unitOfWork;
        public TaskService(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.GetRepository<TaskModel>();
        }

        public void CreateTask(TaskDTO task)
        {
            if (task == null) throw new Exception("You can`t create empty task");
            else _repository.Create(_mapper.Map<TaskModel>(task));
        }

        public void DeleteTask(int id)
        {
            if (_repository.Get(id) == null) throw new NotFoundException($"Not found task with id={id}");
            else  _repository.Delete(id);
        }

        public void DeleteTask(TaskDTO task)
        {
            if (_repository.Get(task.Id) == null) throw new NotFoundException($"Not found task with id={task.Id}");
            else _repository.Delete(_mapper.Map<TaskModel>(task));
        }

        public void UpdateTask(TaskDTO task)
        {
            if (_repository.Get(task.Id) == null) throw new Exception($"Not found task with id={task.Id}");
            else 
            {
                var taskEntity = _repository.Get(task.Id);
                taskEntity = _mapper.Map(task,taskEntity);
                _repository.Update(taskEntity); 
                
            }
        }

        public TaskDTO GetTaskById(int id)
        {
            if (_repository.Get(id) == null) throw new Exception($"Not found task with id={id}");
            else return _mapper.Map<TaskDTO>(_repository.Get(id));
        }

        public List<TaskDTO> GetTasks()
        {
            return _mapper.Map<List<TaskDTO>>(_repository.Get());
        }
        //Task 3
        public Dictionary<int, string> GetListFinishedTasksAt2020(int performerId)
        {
            if (_repository.Get().Where(x => x.PerformerId == performerId).ToList().Count == 0) 
                throw new NotFoundException("Task lust for this user is empty");
            return _repository.Get()
                .Where(task => task.PerformerId == performerId && task.FinishedAt.Year == 2020
                && task.State == TaskStateModel.Finished)
                .ToDictionary(task => task.Id, task => task.Name);
        }
        //Task 2
        public List<TaskDTO> GetTasksForUser(int performerId)
        {
            var data = _repository.Get();
            List<TaskModel> tasks = data.Where(task => task.PerformerId == performerId && task.Name.Length < 45)
                .ToList();
            return _mapper.Map<List<TaskDTO>>(tasks);
        }
       
        public void Dispose()
        {

        }
    }
}
