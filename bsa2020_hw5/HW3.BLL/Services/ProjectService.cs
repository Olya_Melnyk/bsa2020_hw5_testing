﻿using System;
using HW3.DAL.Repositories;
using HW3.DAL.Models;
using System.Collections.Generic;
using System.Linq;
using HW3.BLL.ServicesAbstraction;
using AutoMapper;
using HW3.Common.DTO;
using HW3.DAL.Abstracts;
using HW3.Common.Exeptions;

namespace HW3.BLL.Services
{
    public class ProjectService:IProjectService
    {
        readonly IRepository<Project> _repository;
        readonly IUnitOfWork _unitOfWork;
        readonly IMapper _mapper;
        readonly IRepository<TaskModel> taskRepository;
        readonly IRepository<Team> teamRepository;
        readonly IRepository<User> userRepository;

        public ProjectService(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.GetRepository<Project>();
            taskRepository = _unitOfWork.GetRepository<TaskModel>();
            userRepository = _unitOfWork.GetRepository<User>();
            teamRepository = _unitOfWork.GetRepository<Team>();
            _mapper = mapper;
        }

        public ProjectDTO GetProjectById(int id)
        {
            if (_repository.Get(id) == null) throw new Exception($"Not found project with id={id}");
            else return _mapper.Map<ProjectDTO>(_repository.Get(id));
        }

        public List<ProjectDTO> GetProjects()
        {
            return _mapper.Map<List<ProjectDTO>>(_repository.Get());
        }

        public void DeleteProject(int id)
        {
            if (_repository.Get(id) == null) throw new Exception($"Not found project with id={id}");
            else
            {
                var tasks = taskRepository.Get().Where(x => x.ProjectId == id).ToList();
                for (int i = 0; i < tasks.Count(); i++)
                {
                    taskRepository.Delete(tasks[i].Id);
                }
                _repository.Delete(id);
            }
        }

        public void DeleteProject(ProjectDTO project)
        {
            if (_repository.Get(project.Id) == null)
                throw new Exception($"Not found project with id={project.Id}");
            else
            {
               
                _repository.Delete(_mapper.Map<Project>(project));
            }
        }

        public void DeleteTasksByProjectId(int id)
        {
            var tasks = taskRepository.Get().Where(x => x.ProjectId == id).ToList();
            
            for (int i = 0; i < tasks?.Count(); i++)
            {
                taskRepository.Delete(tasks[i].Id);
            }
        }
        public void UpdateProject(ProjectDTO project)
        {
            if (_repository.Get(project.Id) == null) throw new Exception($"Not found project with id={project.Id}");

            else
            {
                var projectEntity = _repository.Get(project.Id);
                projectEntity = _mapper.Map(project, projectEntity);
                _repository.Update(projectEntity);
            }
        }

        public void CreateProject(ProjectDTO project)
        {
            if (project == null) throw new Exception("You can`t create empty project");
            else _repository.Create(_mapper.Map<Project>(project));
        }
        //Task 1
        public Dictionary<int, int> GetCountTasksByUser(int authorId)
        {
            if (_repository.Get().Count == 0) throw new NotFoundException("Project list is empty");
            return _repository.Get()
                .Where(x => x.AuthorId == authorId)
                .GroupJoin(taskRepository.Get(),
                p => p.Id,
                t => t.ProjectId,
                (p, t) => new {
                    projectId = p.Id,
                    taskListCount = t.Count()
                })
                .ToDictionary(g => g.projectId, g => g.taskListCount);
        }
        //task 7
        public List<AboutProjectDTO> GetInfoAboutProjects()
        {
            var projectList = _repository.Get();
            if (projectList.Count == 0) throw new NotFoundException("Project list is empty");

            var teamList = teamRepository.Get();

            var userList = userRepository.Get();
            var taskList = taskRepository.Get();
            var aboutProject = teamList
                .GroupJoin(
                userList,
                t => t.Id,
                u => u.TeamId,
                (team, u) =>
                {
                    team.UserList = u.ToList();
                    return team;
                })
                .Join(
                projectList,
                t => t.Id,
                p => p.TeamId,
                (t, project) =>
                {
                    project.Team = t;
                    return project;
                })
                .GroupJoin(
                taskList,
                p => p.Id,
                t => t.ProjectId,
                (project, t) =>
                {
                    project.Tasks = t.ToList();
                    return project;
                })
                .Select(
                project => new AboutProject()
                {
                    Project = project,
                    TheLongestTask = project
                    .Tasks?
                    .OrderByDescending(t => t.Description.Length)
                    .FirstOrDefault(),
                    TheShortestTask = project
                    .Tasks?
                    .OrderBy(t => t.Name.Length)
                    .FirstOrDefault(),
                    CountPlayers = project.Tasks.Count < 3
                    || project.Description.Length > 20
                    ? project.Team.UserList?.Count : 0
                }).OrderBy(p => p.Project.Id).ToList();

            return _mapper.Map<List<AboutProjectDTO>>(aboutProject);
        }
        public void Dispose()
        {

        }
    }
}
