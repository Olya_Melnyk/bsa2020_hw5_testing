﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HW3.Common.DTO;
using HW3.BLL.ServicesAbstraction;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HW3.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;
        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }
        [HttpGet]
        public ActionResult<List<TeamDTO>> GetAllTasks()
        {
            try
            {
                return new JsonResult(_teamService.GetTeams());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                return new JsonResult(_teamService.GetTeamById(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        public IActionResult Post([FromBody] TeamDTO team)
        {
            try
            {
                _teamService.CreateTeam(team);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
        [HttpPut]
        public IActionResult UpdateTeam([FromBody] TeamDTO team)
        {
            try
            {
                _teamService.UpdateTeam(team);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete("{id:int}")]
        public IActionResult DeleteTeam(int id)
        {
            try
            {
                _teamService.DeleteTeam(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("users")]//task 4
        public ActionResult<List<TeamPlayersDTO>> GetListUserByTeamAndMoreThenTenYearsOld()
        {
            try
            {
                return new JsonResult(_teamService.GetListUserByTeamAndMoreThenTenYearsOld());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}

