﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Newtonsoft.Json;
using HW3.DAL.Models;
using System.Reflection.Metadata.Ecma335;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore;
using HW3.DAL.Abstracts;

namespace HW3.DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T:TEntity
    {
        private readonly Context context;
        
        public Repository(Context context) 
        {
            this.context = context;
        }
        public List<T> Get(Func<T, bool> filter = null)
        {
            var list = context.Set<T>();
            if (filter != null)
                return list?.Where(filter).ToList();

            else return list?.ToList();
        }
        public void Delete(T entity)
        {
            var set = context.Set<T>();
            if(context.Entry(entity).State==EntityState.Detached)
            {
                set?.Attach(entity);
            }
            set?.Remove(entity);
        }
        public void Create(T entity, string createBody=null)
        {
            context.Set<T>().Add(entity);
            context.SaveChanges();
        }

        public T Get(int id)
        {
            return context.Set<T>().Find(id);
        }

        public  void Update(T entity, string updateBody=null)
        {
            context.Set<T>().Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            T entity = context.Set<T>().Find(id);
            Delete(entity);
        }

        
    }

}
